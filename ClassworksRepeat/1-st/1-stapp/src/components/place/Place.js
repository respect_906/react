import React, {Component} from 'react';

class Place extends Component {
    state = {
        isOpen: false
    };

    render() {
        const {name, info, img, allimg, children} = this.props;
        const {isOpen} = this.state;
        return (
            <div className='cat__item'>
                <h3 className="cat__name">{name}</h3>


                <div className="cat__img-wrapper">
                    <img src={img} alt="Pretty place" className='cat__img'/>
                </div>
                {children}
                <button className="cat__info-toggle" onClick={() => this.toggleInfo()}>Details</button>
                <p>{isOpen && allimg}</p>
                <p>{isOpen && info}</p>
            </div>
        );
    }
    toggleInfo() {
        this.setState({isOpen: !this.state.isOpen});
    }

}

export default Place;