
import './App.css';
import PlaceList from "./components/placeList/PlaceList";

function App() {
  return (
    <div className="App">
      <PlaceList />
    </div>
  );
}

export default App;
