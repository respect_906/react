import {Modal} from "react-bootstrap";
import React from "react";

function MyVerticallyCenteredModal ({show, onHide, header, isCloseIcon, text, buttonOk, buttonCancel}) {

    return (
        <Modal
            show={show}
            onHide={onHide}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            {isCloseIcon&&<Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    {header}
                </Modal.Title>
            </Modal.Header>||<Modal.Header>
                <Modal.Title id="contained-modal-title-vcenter">
                    {header}
                </Modal.Title>
            </Modal.Header>}
            <Modal.Body>
                <p>
                    {text}
                </p>
            </Modal.Body>
            <Modal.Footer>
                {buttonOk}
                {buttonCancel}
            </Modal.Footer>
        </Modal>
    );
}
export default MyVerticallyCenteredModal;