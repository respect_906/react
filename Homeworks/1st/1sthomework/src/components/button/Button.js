import React from 'react';
import {Button} from 'react-bootstrap';
const MyButton = ({backgroundColor, text, onClick}) => {
    return (
        <>
            <Button style={{backgroundColor: backgroundColor}} onClick={onClick}>
                {text}
            </Button>
        </>
    );
};

export default MyButton;