import 'bootstrap/dist/css/bootstrap.min.css';
import MyButton from "./components/button/Button";
import MyVerticallyCenteredModal from './components/modal/Modal'
import React, {useState} from 'react';
import './App.css';
import {Button} from "react-bootstrap";


function App() {
    const [modalShow, setModalShow] = useState(false);
    const [buttonClicked, setButtonClicked] = useState(null);
    console.log(buttonClicked);
    console.log(modalShow);
    const button1Props = {
        backgroundColor: 'green',
        text: 'Open first modal',
        onClick: () => {setModalShow(true); setButtonClicked(0)}
    };
    const button2Props = {
        backgroundColor: 'red',
        text: 'Open second modal',
        onClick: () => {setModalShow(true); setButtonClicked(1)}
    };
    const modal1Props = {
        header: 'Header of the 1-st modal window',
        isCloseIcon: true,
        text: 'Once you delete this file, it won’t be possible to undo this action. Are You sure you want to delete it?',
        buttonOk: <Button class="primary" className='red-button'>Ok</Button>,
        buttonCancel: <Button class="primary" className='red-button'>Cancel</Button>,
    }  ;
    const modal2Props = {
        header: 'Header of the 2-nd modal window',
        isCloseIcon: true,
        text: 'Once you delete this file, it won’t be possible to undo this action. Are You sure you want to delete it?',
        buttonOk: <Button class="primary" className='red-button'>Nehay</Button>,
        buttonCancel: <Button class="primary" className='red-button'>Ne treba</Button>,
    };

    return (
        <>
            <MyButton class="primary" {...button1Props}/>

            <MyButton class="primary"{...button2Props}/>

            {buttonClicked === 0 && <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                {...modal1Props}
            />}
            {buttonClicked === 1 && <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                {...modal2Props}
            />}


        </>
    );



}

export default App;