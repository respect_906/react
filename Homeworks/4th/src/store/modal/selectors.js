export const getIsAddModalOpenedSelector = state => state.modal.isAddOpened;
export const getIsRemoveModalOpenedSelector = state => state.modal.isRemoveOpened;
