import { SET_IS_ADDMODAL_OPENED } from "./types"
import { SET_IS_REMOVEMODAL_OPENED } from "./types"

export const saveIsAddModalOpened = (isAddOpened) => ({type: SET_IS_ADDMODAL_OPENED, payload: isAddOpened});
export const saveIsRemoveModalOpened = (isRemoveOpened) => ({type: SET_IS_REMOVEMODAL_OPENED, payload: isRemoveOpened});

// export const toggleFavoritesAction = (email) => ({type: TOGGLE_FAVORITES, payload: email});
