import { SET_IS_ADDMODAL_OPENED, SET_IS_REMOVEMODAL_OPENED } from "./types"

const initialState = {
  isAddOpened: false,
  isRemoveOpened : false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_IS_ADDMODAL_OPENED : {
      return {...state, isAddOpened:action.payload}
    }
    case SET_IS_REMOVEMODAL_OPENED: {
      return {...state, isRemoveOpened:action.payload }
    }
    default: {
      return state
    }

  }
}
export default reducer;