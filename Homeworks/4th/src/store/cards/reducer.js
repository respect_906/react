import { SET_CARDS, SET_IN_CART, SET_IS_FAVORITE } from "./types"

const initialState = {
  allCards: [],
  inCartCards : {article: ""},
  isFavoriteCards : false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CARDS : {
      return {...state, allCards:action.payload}
    }
    case SET_IN_CART : {
      return {...state,  inCartCards: action.payload }
    }
    case SET_IS_FAVORITE : {
      return {...state,  isFavoriteCards: action.payload }
    }
    default: {
      return state
    }

  }
}
export default reducer;