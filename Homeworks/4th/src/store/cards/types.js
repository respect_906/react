export const SET_CARDS = 'SET_CARDS';
export const SET_IN_CART = 'SET_IN_CART';
export const SET_IS_FAVORITE = 'SET_IS_FAVORITE';
