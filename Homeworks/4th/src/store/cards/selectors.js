export const getCardsSelector = state => state.cards.allCards;
export const getInCartCardSelector = state => state.cards.inCartCards;
export const getIsFavoriteCardSelector = state => state.cards.isFavoriteCards;