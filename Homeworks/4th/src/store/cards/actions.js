import { SET_CARDS, SET_IN_CART, SET_IS_FAVORITE } from "./types"

export const saveCardsAction = (allCards) => ({type: SET_CARDS, payload: allCards});
export const saveInCartAction = (inCartCards) => ({type: SET_IN_CART, payload: inCartCards});
export const saveIsFavoriteCardAction = (inFavoriteCards) => ({type: SET_IS_FAVORITE, payload: inFavoriteCards});

// export const toggleFavoritesAction = (email) => ({type: TOGGLE_FAVORITES, payload: email});
