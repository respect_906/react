import { SET_CURRENT_USER } from "./types";

export const saveCurrentUserAction = (currentUser) => ({type: SET_CURRENT_USER, payload: currentUser});
