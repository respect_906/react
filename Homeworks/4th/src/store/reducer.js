import { combineReducers } from 'redux';
import cards from './cards/reducer';
import modal from './modal/reducer';
import user from './user/reducer'
const reducer = combineReducers({
  cards,
  modal,
  user
});

export default reducer;
