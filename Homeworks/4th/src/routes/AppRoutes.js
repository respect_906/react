import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import {AllCardItems} from "../components/AllCardItems/AllCardItems";
import {Favorite} from "../pages/Favorite/Favorite";
import {Cart} from "../pages/Cart/Cart";
import CardPage from "../pages/cardPage/CardPage";
import Page404 from "../pages/Page404/Page404";
import LoginForm from "../components/LoginForm/LoginForm";
import PageNonAuth from "../pages/PageNonAuth/PageNonAuth"
import SignUpPage from "../pages/signUpPage/SignUpPage";

const AppRoutes = () => {
    return (
        <Switch>
            <Route exact path="/login"
                   render={() => <LoginForm/>}/>
            <Route exact path="/"
                   render={() => <AllCardItems/>}/>
            <Route exact path="/all"><Redirect to="/"/></Route>
            <Route exact path="/favorite"
                   render={() => <Favorite/>}/>
            <Route exact path="/cart"
                   render={() => <Cart/>}/>
            <Route exact path="/every/:id">
                <CardPage/>
            </Route>
          <Route exact path="/notAuth" render={() =><PageNonAuth/>}/>
          <Route exact path="/signup" render={() =><SignUpPage/>}/>
            <Route path="*" component={Page404}/>
        </Switch>
    );
};

export default AppRoutes;