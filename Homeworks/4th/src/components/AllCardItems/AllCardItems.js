import React, {useEffect} from 'react';
import {CardItem} from "../CardItem/CardItem";
import PropTypes from 'prop-types';
import "./allcardItems.scss";

import {getCardsSelector} from "../../store/cards/selectors"
import { useDispatch, useSelector } from 'react-redux';
import { getCardsOperation as getCards } from '../../store/cards/operations';

export const AllCardItems = () => {
    const dispatch = useDispatch();
    const allCards = useSelector(getCardsSelector);
    useEffect(() => {
            if (!allCards.length) {
                dispatch(getCards())};
    }, [dispatch]);
    const AllCardItems = allCards.map(item => <CardItem  {...item} key={item.article}
    />);
    return (
        <div className="card-items">
            {AllCardItems}
        </div>
            )
};

AllCardItems.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object)
};

