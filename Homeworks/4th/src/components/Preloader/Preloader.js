import React from 'react';
import "./Preloader.scss"

export const Preloader = () => {
    return (
        <div style={{textAlign: "center"}}>
            <img className="preloader" src="/img/logo.svg" alt="111"/>
            <h1>Site is loading ...</h1>
        </div>
    );
};
