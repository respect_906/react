import React, {useEffect, useRef, useState} from 'react';
import "./card.scss";
import StarIcon from "../StarIcon/StarIcon";
import {TweenMax, TimelineLite, Power3} from "gsap";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {saveIsAddModalOpened} from "../../store/modal/actions"
import {saveIsRemoveModalOpened} from "../../store/modal/actions"
import {saveInCartAction} from "../../store/cards/actions";
import {saveIsFavoriteCardAction} from "../../store/cards/actions"
import {getIsFavoriteCardSelector} from "../../store/cards/selectors"

export const CardItem = ({title, price, article, img, color}) => {
  const dispatch = useDispatch()
  const [isFavorite, setIsFavorite] = useState(JSON.parse(localStorage.getItem(`Product  ${article}  isFavorite`)));
  // const isFavoriteInRedux = useSelector(getIsFavoriteCardSelector)
  // const isFavorite = JSON.parse(localStorage.getItem(`Product  ${article}  isFavorite`));
  // useEffect(() => dispatch(saveIsFavoriteCardAction(isFavorite)))
  // dispatch(saveIsFavoriteCardAction(isFavorite)
  const inCart = JSON.parse(localStorage.getItem(`CardCart article:${article}`));

  const handleChange = () => {
    setIsFavorite(!isFavorite);
    localStorage.setItem(`Product  ${article}  isFavorite`, !isFavorite);
    dispatch(saveIsFavoriteCardAction(isFavorite));
  };
  const handleModalAddOpen = () => {
    dispatch(saveIsAddModalOpened(true));
    dispatch(saveInCartAction({article}));
  };
  const handleModalRemoveOpen = () => {
    dispatch(saveIsRemoveModalOpened(true));
    dispatch(saveInCartAction({article}));
  }
  const card = useRef(null);
  useEffect(() => {
    const tl = new TimelineLite();
    TweenMax.to(card.current, 0, {css: {visibility: 'visible'}});
    tl.from(card.current, 1, {y: 200, ease: Power3.easeOut});
  }, []);
  const link = `/every/${article}`;
  return (
    <div className="card-wrapper" ref={card}>
      <Link to={link} className="card-wrapper__link">
        <div className="card-wrapper__img"
             style={{backgroundImage: `url(${img})`, backgroundSize: 'contain', backgroundRepeat: 'no-repeat'}}>
        </div>
        <p>{title}</p>
        <p>{price}</p>
        <p>{article}</p>
        <p>{color}</p>
      </Link>
      {
        !inCart &&
        <button type="button" className="card-wrapper__btn"
                onClick={() => {
                  handleModalAddOpen(article)
                }}>Add to cart</button>
      }
      {
        inCart &&
        <button type="button" className="card-wrapper__btn"
                onClick={() => {
                  handleModalRemoveOpen(article)
                }}>Remove from cart
        </button>
      }
      <div onClick={handleChange}>
        <StarIcon color='red' filledColor="gold" isActive={isFavorite}/>
      </div>
    </div>
  );
};

CardItem.defaultProps = {
  color: "Your unique color ( :",
  reload: true
};