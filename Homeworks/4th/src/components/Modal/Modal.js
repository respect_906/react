import React, {useEffect, useRef} from 'react';
import './modal.scss';
import {TweenMax, TimelineLite, Power3} from "gsap";
import {useSelector, useDispatch} from "react-redux";
import {getInCartCardSelector} from "../../store/cards/selectors"
import {saveIsAddModalOpened, saveIsRemoveModalOpened} from "../../store/modal/actions"

export const Modal = ({propses}) => {
    const dispatch = useDispatch();
    const inCartCard = useSelector(getInCartCardSelector);
    const handleFormOk = () => {
        localStorage.setItem(`CardCart article:${inCartCard.article}`, propses.inCart);
        dispatch(saveIsAddModalOpened(false));
         dispatch(saveIsRemoveModalOpened(false));
    };
    const handleFormCancel = () => {
        dispatch(saveIsAddModalOpened(false));
        dispatch(saveIsRemoveModalOpened(false));
    };
    const mod = useRef(null);
    const tl = new TimelineLite();
    useEffect(()=> {
        TweenMax.to(mod.current, 0,{css: {visibility: 'visible'}});
        tl.from(mod.current,1.2,{x:1280, ease:Power3.easeOut});
    });
    return (
        <>
            <div className="fixed-overlay"  onClick={handleFormCancel}>
            </div>
            <div className='modal' ref={mod}>
                <p>{propses.text}</p>
                <button type="button" className="modal-button" onClick={handleFormOk}>Ok</button>
                <button type="button" className="modal-button" onClick={handleFormCancel}>Cancel</button>
            </div>
        </>
    );
};
