import React, {useEffect} from 'react';
import {CardItem} from "../../components/CardItem/CardItem";
import {useParams, withRouter} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getCardsSelector} from "../../store/cards/selectors";
import {getCardsOperation as getCards} from "../../store/cards/operations";

const CardPage = ({handleModalAddOpen, handleModalRemoveOpen}) => {
    const {id} = useParams();
    const dispatch = useDispatch();
    const cardItems = useSelector(getCardsSelector);
    useEffect(() => {
        if (!cardItems.length)
        {dispatch(getCards())};
    }, [dispatch]);
    const cardItem = cardItems.map(item => {
        if (item.article === id) {
           return (<CardItem {...item} key={item.article} handleModalAddOpen={handleModalAddOpen} handleModalRemoveOpen={handleModalRemoveOpen}/>)
        }
        else {
            return null
        }
    });
    return (
        <div className="card-items">
        {cardItem}
        </div>
    );
};
export default withRouter(CardPage)