import React, {useEffect} from 'react';
import {CardItem} from "../../components/CardItem/CardItem";
import "../../components/AllCardItems/allcardItems.scss"
import {useDispatch, useSelector} from "react-redux";
import {getCardsSelector} from "../../store/cards/selectors";
import {getCardsOperation as getCards} from "../../store/cards/operations";

export const Favorite = () => {
    const dispatch = useDispatch();
    const cartItems = useSelector(getCardsSelector);
    useEffect(() => {
        if (!cartItems.length)
        {dispatch(getCards())};
    }, [dispatch]);
    const cardItems = cartItems.map(item => {
        const i = item.article;
        if (JSON.parse(localStorage.getItem(`Product  ${i}  isFavorite`))) {
            return (
                <CardItem {...item} key={i}/>
            )
        }
        else return null
    });
    return (
        <div className="card-items">
            {cardItems}
        </div>
    );
};
