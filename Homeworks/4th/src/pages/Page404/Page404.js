import React from 'react';

const Page404 = () => {
    return (
        <h1>
            OOooppps, error 404! There is no such an page!
        </h1>
    );
};

export default Page404;