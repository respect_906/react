import React, {useEffect, useState} from 'react';
import {CardItem} from "../../components/CardItem/CardItem";
import "../../components/AllCardItems/allcardItems.scss"
import {useDispatch, useSelector} from "react-redux";
import {getCardsSelector} from "../../store/cards/selectors";
import {getCardsOperation as getCards} from "../../store/cards/operations";

export const Cart = () => {
    // const [reload, setReload] = useState(false);
    const dispatch = useDispatch();
    const cartItems = useSelector(getCardsSelector);
    useEffect(() => {
        if (!cartItems.length)
        {dispatch(getCards())};
    }, [dispatch]);

    const inCartItems = cartItems.map(item => {
        const i = item.article;
        if (JSON.parse(localStorage.getItem(`CardCart article:${i}`))) {
            return (
                <CardItem {...item} key={i}
                          // reload={reload}
                          // setReload={setReload}
                          // handleModalRemoveOpen={handleModalRemoveOpen}
                />
            )
        }
        else {
            return null
        }
    });
    return (
        <div className="card-items">
            {inCartItems}
        </div>
    );
};
