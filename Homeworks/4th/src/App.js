import './App.scss';
import React from 'react'
import {Modal} from "./components/Modal/Modal"
import {Header} from "./components/Header/Header";
import AppRoutes from "./routes/AppRoutes";
import {useSelector} from "react-redux";
import {getIsAddModalOpenedSelector} from "./store/modal/selectors"
import {getIsRemoveModalOpenedSelector} from "./store/modal/selectors"

function App() {
    const isModalAddOpened = useSelector(getIsAddModalOpenedSelector);
    const isModalRemoveOpened = useSelector(getIsRemoveModalOpenedSelector);
    const modalAddToCartProps = {
        text: "Are You sure, You want to add this product to cart?",
        inCart: true,
    };
    const modalRemoveFromCartProps = {
        text: "Are You sure, You want to remove this product from cart?",
        inCart: false
    };
    return (
        <div className="App">
            <Header/>
            <AppRoutes
            />
            {isModalAddOpened && <Modal
                propses={modalAddToCartProps}
            />
            }
            {isModalRemoveOpened && <Modal
                propses={modalRemoveFromCartProps}
            />
            }
        </div>
    );
}

export default App;


