import React, {useEffect, useRef, useState} from 'react';
import "./card.scss";
import StarIcon from "../StarIcon/StarIcon";
import {TweenMax, TimelineLite, Power3} from "gsap";
import {Link} from "react-router-dom";

export const CardItem = ({handleModalAddOpen, handleModalRemoveOpen, title, price, article, img, color, setReload, reload}) => {
    const [isFavorite, setIsFavorite] = useState(JSON.parse(localStorage.getItem(`Product  ${article}  isFavorite`)));
    const [inCart, setInCart] = useState(false);
    const handleChange = () => {
        setIsFavorite(!isFavorite);
        localStorage.setItem(`Product  ${article}  isFavorite`, !isFavorite);
        setReload(!reload);
    };
    const card = useRef(null);
  useEffect(() => {
    setInCart(JSON.parse(localStorage.getItem(`CardCart  article:${article}`)))
  });

    useEffect(()=> {
        const tl = new TimelineLite();
        TweenMax.to(card.current, 0,{css: {visibility: 'visible'}});
        tl.from(card.current,1,{y:200, ease:Power3.easeOut});
    },[]);
    const link = `/every/${article}`;
    return (
        <div className="card-wrapper" ref={card}>
            <Link to={link} className="card-wrapper__link">
            <div className="card-wrapper__img"
                 style={{backgroundImage: `url(${img})`, backgroundSize: 'contain', backgroundRepeat: 'no-repeat'}}>
            </div>
            <p>{title}</p>
            <p>{price}</p>
            <p>{article}</p>
            <p>{color}</p>
            </Link>
            {
                !inCart &&
                <button type="button" className="card-wrapper__btn" onClick={() => {handleModalAddOpen(article)}}>Add to cart</button>
            }
            {inCart &&
            <button type="button" className="card-wrapper__btn"
                    onClick={() => {handleModalRemoveOpen(article)}}>Remove from cart
            </button>
            }
            <div onClick={handleChange}>
                <StarIcon color='red' filledColor="gold" isActive={isFavorite}/>
            </div>
        </div>
    );
};

CardItem.defaultProps = {
    color: "Your unique color ( :",
    reload: true
};