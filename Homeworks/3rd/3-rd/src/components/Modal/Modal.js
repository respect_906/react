import React, {useEffect, useRef} from 'react';
import './modal.scss';
import {TweenMax, TimelineLite, Power3} from "gsap";

export const Modal = ({currentCard, handleModalClose, propses}) => {
    const handleFormOk = () => {
        localStorage.setItem(`CardCart  article:${currentCard}`, propses.inCart);
        handleModalClose();
    };
    const handleFormCancel = () => {
        handleModalClose();
    };
    const mod = useRef(null);
    const tl = new TimelineLite();
    useEffect(()=> {
        TweenMax.to(mod.current, 0,{css: {visibility: 'visible'}});
        tl.from(mod.current,1.2,{x:1280, ease:Power3.easeOut});
    });
    return (
        <>
            <div className="fixed-overlay"  onClick={handleFormCancel}>
            </div>
            <div className='modal' ref={mod}>
                <p>{propses.text}</p>
                <button type="button" className="modal-button" onClick={handleFormOk}>Ok</button>
                <button type="button" className="modal-button" onClick={handleFormCancel}>Cancel</button>
            </div>
        </>
    );
};
