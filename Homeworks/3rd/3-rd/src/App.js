import './App.scss';
import React, {useState, useEffect} from 'react'
import {Modal} from "./components/Modal/Modal"
import {Header} from "./components/Header/Header";
import {Preloader} from "./components/Preloader/Preloader";
import AppRoutes from "./routes/AppRoutes";
import axios from "axios";

function App() {
    const [currentUser, setCurrentUser] = useState(null);
    const [items, setItems] = useState([]);
    const [isModalAddOpened, setIsModalAddOpened] = useState(false);
    const [isModalRemoveOpened, setIsModalRemoveOpened] = useState(false);
    const [currentCard, setCurrentCard] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    const modalAddToCartProps = {
        text: "Are You sure, You want to add this product to cart?",
        inCart: true,
    };
    const modalRemoveFromCartProps = {
        text: "Are You sure, You want to remove this product from cart?",
        inCart: false
    };
    useEffect(() => {
        // This is for server

        // axios.get("/all")
        //   .then(res => {setItems(res.data.items);
        //   setTimeout(() => setIsLoading(false, 2000))})

        //This is for local file

        fetch('/items.json')
            .then(res => res.json())
            .then(data => setItems(data.items)).then(console.log(items.find(art=> art.article === "5")))
            .then(setTimeout(() => setIsLoading(false), 3000))
    }, []);
    const handleModalAddOpen = (currCard) => {
        setIsModalAddOpened(!isModalAddOpened);
        setCurrentCard(currCard);
    };
    const handleModalRemoveOpen = (currCard) => {
        setIsModalRemoveOpened(!isModalRemoveOpened);
        setCurrentCard(currCard);
    };
    const handleModalClose = () => {
        setIsModalAddOpened(false);
        setIsModalRemoveOpened(false);
    };


    if (isLoading) {
        return (
            <Preloader/>
        )
    }
    return (
        <div className="App">
            <Header currentUser={currentUser}/>
            <AppRoutes items={items}
                       currentUser={currentUser}
                       setCurrentUser={setCurrentUser}
                       handleModalAddOpen={(nS) => handleModalAddOpen(nS)}
                       handleModalRemoveOpen={(nS) => handleModalRemoveOpen(nS)}
            />
            {isModalAddOpened && <Modal
                propses={modalAddToCartProps}
                currentCard={currentCard}
                handleModalClose={() => handleModalClose()}/>
            }
            {isModalRemoveOpened && <Modal
                propses={modalRemoveFromCartProps}
                currentCard={currentCard}
                handleModalClose={() => handleModalClose()}/>
            }
        </div>
    );
}

export default App;


