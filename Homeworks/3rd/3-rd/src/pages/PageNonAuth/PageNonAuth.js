import React from 'react';

const PageNonAuth = () => {
  return (
    <h1>
      Sorry, there is no such an user! Try to input valid user name and password
    </h1>
  );
};

export default PageNonAuth;