import React from 'react';
import LoginForm from "../../components/LoginForm/LoginForm";

const Login = ({auth}) => {
    return (
            <LoginForm auth={auth}/>
    );
};

export default Login;