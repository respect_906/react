import React, {useState} from 'react';
import {CardItem} from "../../components/CardItem/CardItem";
import "../../components/CardItems/cardItems.scss"

export const Cart = ({items, handleModalRemoveOpen}) => {
    const [reload, setReload] = useState(false);
    const cardItems = items.map(item => {
        const i = item.article;
        if (JSON.parse(localStorage.getItem(`CardCart  article:${i}`))) {
            return (
                <CardItem {...item} key={i}
                          reload={reload}
                          setReload={setReload}
                          handleModalRemoveOpen={handleModalRemoveOpen}
                />
            )
        }
        else {
            return null
        }
    });
    return (
        <div className="card-items">
            {cardItems}
        </div>
    );
};
