import React, {Component} from 'react';
import CardItem from "../CardItem/CardItem";
import PropTypes from 'prop-types';

class CardItems extends Component {
    render() {
        const {items, handleModalOpen} = this.props;
        return (
            items.map((item) =>
                    <CardItem {...item} key={item.article} handleModalOpen={handleModalOpen}/>
            ));
    }
}
export default CardItems;

CardItems.propTypes = {
        items : PropTypes.array
};

