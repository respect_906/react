import React, {Component} from 'react';
import './modal.scss';

class Modal extends Component {
    handleFormOk = () => {
        const {currentCard, handleModalClose} = this.props;
        localStorage.setItem(`CardCart  article:${currentCard}`, "just added");
        handleModalClose()
    };
    handleFormCancel = () => {
        const {handleModalClose} = this.props;
        handleModalClose()
    };

    render() {
        return (
            <>
                <div className="fixed-overlay" onClick={this.handleFormCancel}>
                </div>
                <div className='modal'>
                    <p>Are You sure You want to add this product to cart?!</p>
                    <button type="button" className="modal-button" onClick={this.handleFormOk}>Ok</button>
                    <button type="button" className="modal-button" onClick={this.handleFormCancel}>Cancel</button>
                </div>
            </>
        );
    }
}

export default Modal;
