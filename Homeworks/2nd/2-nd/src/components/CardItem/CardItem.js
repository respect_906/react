import React, {Component} from 'react';
import "./card.scss";
import StarIcon from "../StarIcon/StarIcon";

class CardItem extends Component {
    state = {
        isFavorite: JSON.parse(localStorage.getItem(`Product  ${this.props.article}  isFavorite`))
    };
    handleChange = () => {
        const {article} = this.props;
        const {isFavorite} = this.state;
        this.setState({isFavorite: !isFavorite});
        localStorage.setItem(`Product  ${article}  isFavorite`, !isFavorite);
    };

    render() {
        const {isFavorite} = this.state;
        const {handleModalOpen, title, price, article, img, color} = this.props;
        return (

            <div className="card-wrapper">
                <div className="card-wrapper__img"
                     style={{backgroundImage: `url(${img})`, backgroundSize: 'contain', backgroundRepeat: 'no-repeat'}}>
                </div>
                <p>{title}</p>
                <p>{price}</p>
                <p>{article}</p>
                <p>{color}</p>
                <button type="button" className="card-wrapper__btn-addtocart"
                        onClick={() => handleModalOpen(article)}>Add to cart
                </button>
                {/*<input type="checkbox" className="star" checked={isFavorite} onChange={this.handleChange}/>*/}
                <div onClick={this.handleChange}>
                    <StarIcon color='red' filledColor="gold" isActive={isFavorite}/>
                </div>
            </div>

        );


    }
}

export default CardItem;
CardItem.defaultProps = {
    color: "black"
};