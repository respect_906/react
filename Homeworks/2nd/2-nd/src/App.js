import './App.scss';
import React, {Component} from 'react'
import CardItems from "./components/CardItems/CardItems";
import Modal from "./components/Modal/Modal"

class App extends Component {

    state = {
        items: [],
        isModalOpened: false,
        currentCard: {},
    };

    handleModalOpen(currCard) {
        this.setState((state) => ({isModalOpened: !state.isModalOpened, currentCard: currCard}))
    }

    handleModalClose() {
        this.setState({isModalOpened: false})
    }

    componentDidMount() {
        fetch('items.json')
            .then(res => res.json())
            .then(data => {
                this.setState(data)
            })
    }

    render() {
        const {items, isModalOpened, currentCard} = this.state;
        return (
            <div className="App">
                <CardItems items={items} handleModalOpen={(nS) => this.handleModalOpen(nS)}
                           handleFavorite={(nF) => this.handleFavorite(nF)}/>
                {isModalOpened && <Modal currentCard={currentCard} handleModalClose={() => this.handleModalClose()}/>}
            </div>
        );
    }
}

export default App

